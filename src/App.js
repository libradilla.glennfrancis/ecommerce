import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { UserProvider } from "./UserContext";
import {useState, useEffect} from "react";

import AppNavbar from "./components/AppNavbar";

import AdminDashboard from "./pages/AdminDashboard"
import Home from "./pages/Home";
import Login from "./pages/Login";
import Product from "./pages/Product";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import ProductView from "./pages/ProductView";
import OrderHistory from "./pages/OrderHistory";
import UserAdmin from "./pages/UserAdmin";
import UserOrders from "./pages/UserOrders";

import {Container} from "react-bootstrap";
import './App.css';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  const unsetUser = () =>{
    localStorage.clear();
  }
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              //undefined
              id: null,
              isAdmin: null
            })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser,unsetUser}}>
    <Router>
      <AppNavbar />

      <Container fluid>
          <Routes>
              <Route exact path = "/" element = {<Home />} />
              <Route exact path = "/login" element = {<Login />} />
              <Route exact path ="/product" element={<Product />} />
              <Route exact path ="/logout" element={<Logout />} />
              <Route exact path ="/register" element={<Register />} />
              <Route exact path ="/product/:productId" element={<ProductView />} />
              <Route exact path ="/admin" element={<AdminDashboard />} />
              <Route exact path ="/orders" element={<OrderHistory />} />
              <Route exact path ="/users" element={<UserAdmin />} />
              <Route exact path ="/userOrder" element={<UserOrders />} />
          </Routes>
      </Container>
    </Router>
    </UserProvider>

  );
}

export default App;
