// import { Link } from "react-router-dom";
// import {useState, useContext} from "react";
import UserContext from "../UserContext"
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import "../styles/navbar.css"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";



export default function AppNavbar(){
	const {user} = useContext(UserContext);



	return(
		<Navbar id="navbar"  expand="lg">
		      <Container>
		        <Navbar.Brand id="navbrand" as={Link} to="/">Beauty Options</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link id="navlink1" as={Link} to="/" eventKey="/">Home</Nav.Link>
		            <Nav.Link id="navlink2" as={Link} to="/product" eventKey="/product">Product</Nav.Link>
		            {	
	            	(user.id !== null)
	            	?	
	            		<>
	            		<Nav.Link id="navlink3" as={Link} to="/userOrder" eventKey="/userOrder">Orders</Nav.Link>
	            		<Nav.Link id="navlink4" as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            		</>
	            	:
	            		<>
		            		<Nav.Link id="navlink5" as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            		<Nav.Link id="navlink6" as={Link} to="/register" eventKey="/register">Register</Nav.Link>
	            		</>
	            	}
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		  );
}
