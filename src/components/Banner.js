import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import "../styles/banner.css"

export default function Banner({data}){
	console.log(data);
	const {title, content, destination, label} = data;
	return (
			<Row>
				<Col className="p-1 text-center">
					<h1 id="homelogo">{title}</h1>
					<p id="tagline">{content}</p>
					<Button id="btnBanner" size="lg" as={Link} to = {destination}>{label}</Button>
					{/*<Button as={Link} to = {destination} variant="primary">{label}</Button>*/}
				</Col>
			</Row>
		)
}