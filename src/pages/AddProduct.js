import {useState, useEffect} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import Swal from "sweetalert2";
import {Navigate, useNavigate} from "react-router-dom";

export default function Example() {
  const [show, setShow] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);
  const [thumbnail, setThumbnail] = useState("");
  const [pic1, setpic1] = useState("");
  const [pic2, setpic2] = useState("");
  const [pic3, setpic3] = useState("");

  function addProduct(e){
  	e.preventDefault();
  	fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
  	  method: "POST",
  	  headers:{
  	    "Authorization": `Bearer ${localStorage.getItem("token")}`,
  	    "Content-Type":"application/json"

  	  },
  	  body: JSON.stringify({
  	      productName: productName,
  	      description: description,
  	      price: price,
  	      stocks: stocks,
          thumbnail: thumbnail,
          pic1: pic1,
          pic2: pic2,
          pic3: pic3
  	  })
  	})
  	.then(res => res.json())
  	.then(data => {
  	  if(data){
  	    Swal.fire({
  	      title: "Product added successfully",
  	      icon: "success",
  	      text: `You have successfully added ${productName}`
  	    })
  	    navigate("/product");

  	  }

  	  else{
  	    Swal.fire({
  	      title: "Something went wrong",
  	      icon: "error",
  	      text: "Please try again."
  	    })
  	  }

  	})
  }

  useEffect(()=>{
      if((productName !== "" && description !== "" && price !== "" && stocks !== ""&& thumbnail!==""&& pic1!==""&& pic2!==""&& pic3!=="")){
        setIsActive(true);
      }
      else{
        setIsActive(false);
      }
    },[productName, description, price, stocks, thumbnail, pic1, pic2,pic3])

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button className="mx-2 my-1" size="lg" variant="primary" onClick={handleShow}>
        Add Product
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header>
          <Modal.Title>Add a Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form >
                  <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Product Name:</Form.Label>
                    <Form.Control type="string" value={productName} onChange={e=>setProductName(e.target.value)}/>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Product Description: </Form.Label>
                    <Form.Control ype="string" value={description} onChange={e=>setDescription(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Price: </Form.Label>
                    <Form.Control type="number" value={price} onChange={e=>setPrice(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Stocks: </Form.Label>
                    <Form.Control type="number" value={stocks} onChange={e=>setStocks(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Thumbnail: </Form.Label>
                    <Form.Control type="string" value={thumbnail} onChange={e=>setThumbnail(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 1: </Form.Label>
                    <Form.Control type="string" value={pic1} onChange={e=>setpic1(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 2: </Form.Label>
                    <Form.Control type="string" value={pic2} onChange={e=>setpic2(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 3: </Form.Label>
                    <Form.Control type="string" value={pic3} onChange={e=>setpic3(e.target.value)} />
                  </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Discard
            </Button>
           {
          (isActive)
          ?
          <Button variant="primary"  onClick ={(e) => addProduct(e)}>Save</Button>
          :
          <Button variant="primary"  onClick ={(e) => addProduct(e)} disabled>Save</Button>
           }
        </Modal.Footer>
      </Modal>
    </>
  );
}