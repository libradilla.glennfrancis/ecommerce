import {useContext, useEffect} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Logout(){
	// localStorage.clear();
	const {setUser, unsetUser} = useContext(UserContext);
	// Clear the localStorage of the  user's information
	unsetUser();
	// console.log(user);
	useEffect(()=>{
		setUser({
			id: null,
			isAdmin: null
		})
	})

	return(
		// Redirect back to the login
		<Navigate to="/login" />
		)
}